import axios from '../utils/config/axios.config';

/**
 * 
 * @param { string } email Email to login a user
 * @param { string }password Password to login a user
 * @returns 
 */

export const login = (email: string, password: string) => {
    
    // * Declare Body to Post
    let body = {  email, password };

    // * Send POST request to login endpoint

    return axios.post('/auth/login', body)
}

/**
 * 
 * @param { string } name name of user
 * @param { string } email email of user
 * @param { string } password password of user
 * @param { number } age age of user
 * @returns 
 */
export const register = (name: string, email: string, password: string, age: number) => {
    
    // * Declare Body to Post
    let body = { name, email, password, age };

    // * Send POST request to register endpoint

    return axios.post('/auth/register', body)
}