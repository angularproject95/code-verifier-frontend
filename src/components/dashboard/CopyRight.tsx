import Typography from "@mui/material/Typography";
import Link from '@mui/material/Link'

export const Copyright = (props: any) => {
    return (
        <Typography variant="body2" color="text.scondary" align="center" {...props}>
            {'Copyright ©'}
            <Link color="inherit" href="https://github.com/lvanegasimagine"> Luis repo &nbsp;</Link>
            { new Date().getFullYear()}
        </Typography>
    )
}