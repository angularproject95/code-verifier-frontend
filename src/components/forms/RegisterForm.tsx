import React from "react";
import * as Yup from "yup";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { register } from "../../services/authService";
import { AxiosResponse } from "axios";

const RegisterForm = () => {
  const initialValues = {
    name: "Luis vanegas",
    email: "lvanegas@imaginegroup.com",
    password: "admin123456",
    confirm: "admin123456",
    age: 18,
  };

  const registerSchema = Yup.object().shape({
    name: Yup.string()
      .min(6, "Username must have 6 letters minimun")
      .max(12, "Username must have maximum 12 letters")
      .required("Username is Required"),
    email: Yup.string()
      .email("Invalid email format")
      .required("Email is Required"),
    password: Yup.string()
      .min(8, "Password too short")
      .required("Password is Required"),
    confirm: Yup.string()
      .when("password", {
        is: (value: string) => (value && value.length > 0 ? true : false),
        then: Yup.string().oneOf([Yup.ref("password")], "Passwords must match"),
      })
      .required("You must confirm your password"),
    age: Yup.number()
      .min(10, "You must be over 10 years old")
      .required("Age is Required"),
  });

  const handleSubmit = async (values: any) => {
    register(values.name, values.email, values.password, values.age)
      .then((response: AxiosResponse) => {
        if (response.status === 201) {
          console.log("User registered correctly");
          console.log(
            "🚀 ~ file: RegisterForm.tsx ~ line 40 ~ register ~ response",
            response.data
          );
        } else {
          throw new Error("Error in Registry");
        }
      })
      .catch((error) => {
        console.log(`[REGISTER ERROR]: Something went Wrong: ${error}`);
      });
  };

  return (
    <div>
      <h4>Register Form</h4>
      <Formik
        initialValues={initialValues}
        validationSchema={registerSchema}
        onSubmit={handleSubmit}
      >
        {({
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          handleBlur,
        }) => (
          <Form>
            <label htmlFor="name">Name: &nbsp;</label>
            <Field id="name" name="name" type="text" placeholder="Your Name" />
            {errors.name && touched.name && (
              <ErrorMessage name="name" component="div" />
            )}
            <label htmlFor="email">Email: &nbsp;</label>
            <Field
              id="email"
              name="email"
              type="email"
              placeholder="example@example.com"
            />
            {errors.email && touched.email && (
              <ErrorMessage name="email" component="div" />
            )}

            <label htmlFor="password">Password: &nbsp;</label>
            <Field
              id="password"
              name="password"
              type="password"
              placeholder="********"
            />
            {errors.password && touched.password && (
              <ErrorMessage name="password" component="div" />
            )}

            <label htmlFor="confirm">Confirm Password: &nbsp;</label>
            <Field
              id="confirm"
              name="confirm"
              type="password"
              placeholder="********"
            />
            {errors.confirm && touched.confirm && (
              <ErrorMessage name="confirm" component="div" />
            )}

            <label htmlFor="age">Age: &nbsp;</label>
            <Field id="age" name="age" type="number" placeholder="Your Age" />
            {errors.age && touched.age && (
              <ErrorMessage name="age" component="div" />
            )}
            <button type="submit">Register</button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default RegisterForm;
