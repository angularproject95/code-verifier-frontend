import React from 'react';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { login } from '../../services/authService';
import { AxiosResponse } from 'axios';
import { Navigate, useNavigate } from 'react-router-dom';

// * Define Schema of validation with Yup

const loginSchema = Yup.object().shape({
    email: Yup.string().email('Invalid Email Format').required('Email is Required'),
    password: Yup.string().required('Password is Required')
});


const LoginForm = () => {

    let navigate= useNavigate();

    // * We define the initial Credentials

    const initialCredentials = {
        email: '',
        password: ''
    }

    const handleSubmit = async (values: any) => {
        login(values.email, values.password).then(async (response: AxiosResponse) =>{
            if(response.status === 200){
                if(response.data.token){
                    await sessionStorage.setItem('sessionJWTToken', response.data.token);
                    navigate("/");
                }else{
                    throw new Error('Error generating Login Token');
                }
            }else{
                throw new Error('Invalid Credentials');
            }
         }).catch((error) => {
            console.log(`[LOGIN ERROR]: Something went Wrong: ${error}`);
        });
    }

    return (
        <div>
            <h4>Login Form</h4>
            <Formik initialValues={initialCredentials} validationSchema={loginSchema}
                    onSubmit={handleSubmit}>
            {
                ({values, touched, errors, isSubmitting, handleChange, handleBlur}) => (
                    <Form>
                        <label htmlFor='email'>Email: </label>
                        <Field id='email' type='email' name='email' placeholder='example@email.com'/>
                        {errors.email && touched.email && (<ErrorMessage name='email' component='div'></ErrorMessage>)}
                        <label htmlFor='password'>Password:</label>
                        <Field id='password' type='password' name='password' placeholder='*******'/>
                        {errors.password && touched.password && (<ErrorMessage name='password' component='div'></ErrorMessage>)}
                        <button type='submit'>Login</button>
                        {isSubmitting && (<p>Checking Credential...</p>)}
                    </Form>
                )
            }
            </Formik>
        </div>
    )
}

export default LoginForm