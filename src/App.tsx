import { BrowserRouter as Router} from "react-router-dom";

import "./App.css";
import { StickyFooter } from "./components/dashboard/StickyFooter";
import { AppRoutes } from "./routes/Routes";

function App() {
  return (
    <div className="App">
      <Router>
        <AppRoutes/>
      </Router>
      <StickyFooter/>
    </div>
  );
}

export default App;
