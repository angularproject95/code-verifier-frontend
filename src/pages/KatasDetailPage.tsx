import { AxiosResponse } from "axios";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Editor } from "../editor/Editor";
import { useSessionStorage } from "../hooks/useSessionStorage";
import { getKataById } from "../services/katasService";
import { Kata } from "../utils/types/Kata.type";

const KatasDetailPage = () => {
  let { id } = useParams();
  let loggedIn = useSessionStorage("sessionJWTToken");
  let navigate = useNavigate();

  const [kata, setKata] = useState<Kata | undefined>(undefined);
  const [showSolution, setShowSolution] = useState(true);

  useEffect(() => {
    if (!loggedIn) {
      return navigate("/login");
    } else {
      if (id) {
        getKataById(loggedIn, id)
          .then((response: AxiosResponse) => {
            if (response.status === 200 && response.data) {
              let kata: Kata = {
                _id: response.data._id,
                name: response.data.name,
                description: response.data.description,
                level: response.data.level,
                intents: response.data.intents,
                stars: response.data.stars,
                creator: response.data.creator,
                solution: response.data.solution,
                participants: response.data.participants,
              };
              setKata(kata);
            }
          })
          .catch((error) => {
            console.error(`[Kata By ID ERROR]: ${error}`);
          });
      } else {
        return navigate("/katas");
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loggedIn]);

  return (
    <div>
      <h1>Katas Detail Page {id}</h1>
      {kata ? (
          <div className="kata-data">
            <h2>{kata?.description}</h2>
            <h3>Rating: {kata.stars}/5</h3>
            <button onClick={() => setShowSolution(!showSolution)}>{showSolution ? 'Hide Solution' : 'Show Solution'}</button>
            {showSolution && <Editor language={'jsx'}>{kata.solution}</Editor>}
          </div>
      ) : (
        <div>
          <h2>Loading data...</h2>
        </div>
      )}

    </div>
  );
};

export default KatasDetailPage;
