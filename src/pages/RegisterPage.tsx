import React from "react";
import RegisterForm from "../components/forms/RegisterForm";
import RegisterMaterial from "../components/forms/RegisterMaterial";

const RegisterPage = () => {
  return (
    <div>
      <h1>Register Page</h1>
      <RegisterMaterial/>
    </div>
  );
};

export default RegisterPage;
