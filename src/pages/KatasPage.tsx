import { AxiosResponse } from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useSessionStorage } from "../hooks/useSessionStorage";
import { getAllKatas } from "../services/katasService";
import { Kata } from "../utils/types/Kata.type";

const KatasPage = () => {
  let loggedIn = useSessionStorage("sessionJWTToken");
  let navigate = useNavigate();
  const [katas, setKatas] = useState([]);
  const [totalPages, setTotalPages] = useState(1);
  const [currentPage, setCurrentPage] = useState(1);

  console.log(
    "🚀 ~ file: KatasPage.tsx ~ line 12 ~ KatasPage ~ katas ACA",
    katas
  );
  console.log(
    "🚀 ~ file: KatasPage.tsx ~ line 13 ~ KatasPage ~ totalPages",
    totalPages
  );
  console.log(
    "🚀 ~ file: KatasPage.tsx ~ line 14 ~ KatasPage ~ currentPage",
    currentPage
  );

  useEffect(() => {
    if (!loggedIn) {
      return navigate("/login");
    } else {
      getAllKatas(loggedIn, 2, 1)
        .then((response: AxiosResponse) => {
          if (
            response.status === 200 &&
            response.data.katas &&
            response.data.totalPages &&
            response.data.currentPage
          ) {
            let { katas, totalPages, currentPage } = response.data;
            setKatas(katas);
            setTotalPages(totalPages);
            setCurrentPage(currentPage);
          } else {
            throw new Error(`Error obtaining katas: ${response.data}`);
          }
        })
        .catch((error: any) => {
          console.error(`[Get All Katas Error] ${error}`);
        });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loggedIn]);

  /**
   * Method to navigate to kata details
   * @param id of Kata to navigate to
   */
  
  const navigateToKataDetail = (id: number) => {
    navigate(`/katas/${id}`);
  };
  
  return (
    <div>
      <h1>Katas Page</h1>
      {katas.length > 0 ? (
        <div>
          <ul>
            {katas.map((kata: Kata) => (
              <div key={kata._id}>
                <h3 onClick={() => navigateToKataDetail(kata._id)} style={{cursor: "pointer"}}>{kata.name}</h3>
                <h4>{kata.description}</h4>
                <h5>Creator: {kata.creator}</h5>
                <p>Rating: {kata.stars}/5</p>
              </div>
            )) }
          </ul>
        </div>
      ) : (
        <div>
          <h5>No hay katas disponibles</h5>
        </div>
      )}
    </div>
  );
};

export default KatasPage;
