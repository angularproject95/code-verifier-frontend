import HomePage from "./HomePage";
import KatasDetailPage from "./KatasDetailPage";
import KatasPage from "./KatasPage";
import LoginPage from "./LoginPage";
import RegisterPage from "./RegisterPage";

export {HomePage, KatasPage, LoginPage, RegisterPage, KatasDetailPage}