import React from "react";
import LoginForm from "../components/forms/LoginForm";
import LoginMaterial from "../components/forms/LoginMaterial";

const LoginPage = () => {
  return (
    <div>
      <h1>Login Page</h1>
      <LoginMaterial/>
    </div>
  );
};

export default LoginPage;
