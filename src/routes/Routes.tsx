import {Routes, Route, Navigate} from 'react-router-dom'
import {
    KatasDetailPage,
    HomePage,
    LoginPage,
    RegisterPage,
    KatasPage,
  } from "../pages";
export const AppRoutes = () => {
    return (
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/register" element={<RegisterPage />} />
          <Route path="/katas" element={<KatasPage />} />
          <Route path="/katas/:id" element={<KatasDetailPage />} />
          <Route path="*" element={<Navigate to="/" replace />} />
        </Routes>
    )
}